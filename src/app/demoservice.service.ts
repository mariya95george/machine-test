import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DemoserviceService {
API_URL = 'https://jsonplaceholder.typicode.com/todos/1';
  constructor(private http: HttpClient) { }
  getTableData(): Observable<any> {
    return this.http.get(this.API_URL);
  }

}
