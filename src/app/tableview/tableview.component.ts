import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DemoserviceService } from '../demoservice.service';
import { TableData } from '../table-data';

@Component({
  selector: 'app-tableview',
  templateUrl: './tableview.component.html',
  styleUrls: ['./tableview.component.css']
})
export class TableviewComponent implements OnInit {
  dataSource = [];
  dataForm: FormGroup;
  hideTable = false;
  formData: TableData = {
    completed: null,
    id: null,
    title: '',
    userId: null
  // tslint:disable-next-line:semicolon
  }
  constructor(private demoService: DemoserviceService, private fb: FormBuilder) { }

 ngOnInit(): void {
  this.createForm();
  this.fetchData();
 }
 // tslint:disable-next-line:typedef
 createForm() {
  this.dataForm = this.fb.group({
    completed: ['', Validators.required],
    id: ['', Validators.required],
    title: ['', Validators.required],
    userid: ['', Validators.required]
  });
 }
 // tslint:disable-next-line:typedef
 submitForm() {
  const submittedResult: TableData = {
    completed : this.formData.completed,
    title: this.formData.title,
    id: this.formData.id,
    userId: this.formData.userId
  };
  console.log('submitted form value', submittedResult);
  this.dataForm.reset();
 }
 // tslint:disable-next-line:typedef
 fetchData() {
  this.demoService.getTableData().subscribe((data) => {
    this.dataSource = data;
    console.log('table data', this.dataSource);
  },
  (error) => {
  });
 }

 // tslint:disable-next-line:typedef
 deleteData() {
 // tslint:disable-next-line:no-string-literal
 delete this.dataSource['completed'];
 delete this.dataSource['id'];
 delete this.dataSource['title'];
 delete this.dataSource['userId']
 console.log('After deletion', this.dataSource);
 }
}
