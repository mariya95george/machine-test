export interface TableData {
    completed: false;
    id: number;
    title: string;
    userId: number;
}
